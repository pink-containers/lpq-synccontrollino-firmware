#include <Controllino.h>
#include "common.hpp"

#define Pump CONTROLLINO_D0
#define Probe CONTROLLINO_D1
#define MainTrigger CONTROLLINO_A0
#define CCD1 CONTROLLINO_A1
#define CCD2 CONTROLLINO_A2

bool CCD1_TTL;
bool CCD2_TTL;
bool CCD_TTL;
bool MainTrig_Status;
bool PumpProbe;
bool doOnce = true;
bool Exp1State;

// bool PumpProbe;

void StartExp1() {
    doOnce = true;
    Exp1State = true;
    PumpProbe = false; //set it false to ensure after it is inverted both lasers fire first time 
    //PumpProbe = !PumpProbe; 
}

void PumpProbeGateINC_CCD1(){
    CCD1_TTL = digitalRead(CCD1);

    if (Exp1State) {
        if (CCD1_TTL && doOnce) {
            PumpProbe = !PumpProbe;
            doOnce = false;
        } else if (!CCD1_TTL) {
            doOnce = true;
        }
    }

    if (CCD1_TTL && Exp1State) {
        if (PumpProbe) {
            digitalWrite(Pump, HIGH);
            digitalWrite(Probe, HIGH);
        } else {
            digitalWrite(Pump, LOW);
            digitalWrite(Probe, HIGH);
        }
    } else {
        digitalWrite(Pump, LOW);
        digitalWrite(Probe, LOW);
    }
}

void StopExp1() {
    Exp1State = false;
}
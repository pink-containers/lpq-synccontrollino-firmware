#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>

#define BUFFER_LEN 32
#define SERVERPORT 23

extern bool Exp1State;

// support functions
unsigned int get_command(EthernetClient* client, char* Buffer, unsigned char MaxLen);
void get_response(char* cmdbuf, char* respbuf, int len);
void mergebits(long* outval, uint8_t input, uint8_t bitpos);
void setupIO(void);
void readallin(long* outval);
void readallout(long* outval);
void PumpProbeGateINC_CCD1(void); //[D]
void StartExp1(void);
void StopExp1(void);
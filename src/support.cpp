#include <Controllino.h>
#include <SPI.h>
#include <Ethernet.h>
#include "common.hpp"

// 32 bit register for status output
long outval;

// monitored controllino inputs
uint8_t allinputs[] = {
  CONTROLLINO_A0,
  CONTROLLINO_A1,
  CONTROLLINO_A2,
  CONTROLLINO_A3,
  CONTROLLINO_A4,
  CONTROLLINO_A5,
  CONTROLLINO_A6,
  CONTROLLINO_A7,
  CONTROLLINO_A8,
  CONTROLLINO_A9,
  CONTROLLINO_A10,
  CONTROLLINO_A11,
  CONTROLLINO_A12,
  CONTROLLINO_A13,
  CONTROLLINO_A14,
  CONTROLLINO_A15,
  CONTROLLINO_I16,
  CONTROLLINO_I17,
  CONTROLLINO_I18,
  CONTROLLINO_IN0,
  CONTROLLINO_IN1
};

// monitored controllino outputs
uint8_t alloutputs[] = {
  CONTROLLINO_D0,
  CONTROLLINO_D1,
  CONTROLLINO_D2,
  CONTROLLINO_D3,
  CONTROLLINO_D4,
  CONTROLLINO_D5,
  CONTROLLINO_D6,
  CONTROLLINO_D7,
  CONTROLLINO_D8,
  CONTROLLINO_D9,
  CONTROLLINO_D10,
  CONTROLLINO_D11,
  CONTROLLINO_D12,
  CONTROLLINO_D13,
  CONTROLLINO_D14,
  CONTROLLINO_D15,
  CONTROLLINO_D16,
  CONTROLLINO_D17,
  CONTROLLINO_D18,
  CONTROLLINO_D19,
  CONTROLLINO_D20,
  CONTROLLINO_D21,
  CONTROLLINO_D22,
  CONTROLLINO_D23,
};

// Helper function to read command and put it in a buffer
unsigned int get_command(EthernetClient* client, char* Buffer, unsigned char MaxLen){
    unsigned int c;
    unsigned char idx = 0;
    
    while(client->available() && idx < MaxLen - 1){
        c = client->read();
        if ((unsigned char)c == '\n'){
            break;
        } else {
            Buffer[idx] = (unsigned char)c;
            idx++;
        }
    }
    Buffer[idx] = '\0';
    return idx;
}

// Simple command mapping
void get_response(char* cmdbuf, char* respbuf, int len){
  bool d0stat;
  bool d1stat;
  if (strcmp(cmdbuf, "statin?")==0){
    readallin(&outval);
    sprintf(respbuf, "statin:%lu\n", outval);
  } else if (strcmp(cmdbuf, "statout?")==0){
    readallout(&outval);
    sprintf(respbuf, "statout:%lu\n", outval);
  } else if (strcmp(cmdbuf, "id?")==0){
    sprintf(respbuf, "Controllino LPS\n");
  } else if (strcmp(cmdbuf, "EXP1:START")==0) {
    StartExp1();
    sprintf(respbuf, "Experiment Started!\n");
  } else if (strcmp(cmdbuf, "EXP1:STOP")==0) {
    StopExp1();
    sprintf(respbuf, "Experiment Ended!\n");

    // TESTING
  } else if (strcmp(cmdbuf, "D4set1")==0){
    digitalWrite(CONTROLLINO_D4, HIGH);
    sprintf(respbuf, "D4 set to 1 value\n");  
  } else if (strcmp(cmdbuf, "D4set0")==0){
    digitalWrite(CONTROLLINO_D4, LOW);
    sprintf(respbuf, "D4 set to 0 value\n");
  } else if (strcmp(cmdbuf, "D0:STAT?")==0){
    d0stat=digitalRead(CONTROLLINO_D0);
    sprintf(respbuf, "D0 status: %d\n", d0stat);  
  } else if (strcmp(cmdbuf, "D1:STAT?")==0){
    d1stat=digitalRead(CONTROLLINO_D1);
    sprintf(respbuf, "D1 status: %d\n", d1stat); 
    // END testing
  } else {
    sprintf(respbuf, "?\n");
  }
}

// add bit value to a 32bit register 
//[D: this function is called within get_response and readallin/out to read A1.. D1 etc channels]
void mergebits(long* outval, uint8_t input, uint8_t bitpos){
  unsigned long i = digitalRead(input);
  *outval |= (i<<bitpos);
}

// read all input values into a 32bit register
void readallin(long* outval){
  *outval=0;
  int inputsize = 21;
  for(int i=0; i<inputsize; i++){
    mergebits(outval, allinputs[i], i);
  }
}

// read all output values into a 32bit register
void readallout(long* outval){
  *outval=0;
  int outputsize = 24;
  for(int i=0; i<outputsize; i++){
    mergebits(outval, alloutputs[i], i);
  }
}

// setup IO direction for inputs and outputs, 
//[D: pinMode configures a specific pin on the microcontroller to behave either as an input or an output]
void setupIO(void){
  int inputsize = 21;
  for(int i; i<inputsize; i++){
    pinMode(allinputs[i], INPUT);
  }
  int outputsize = 24;
  for(int i; i<outputsize; i++){
    pinMode(alloutputs[i], OUTPUT);
  }
}
